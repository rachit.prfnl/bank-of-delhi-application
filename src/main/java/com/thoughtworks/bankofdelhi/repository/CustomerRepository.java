package com.thoughtworks.bankofdelhi.repository;

import com.thoughtworks.bankofdelhi.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRepository extends JpaRepository<Customer, Integer> {

}
