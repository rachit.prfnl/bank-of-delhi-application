package com.thoughtworks.bankofdelhi.controller;

import com.thoughtworks.bankofdelhi.entity.Customer;
import com.thoughtworks.bankofdelhi.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class CustomerController {

    @Autowired
    private CustomerRepository customerRepo;

    @GetMapping("/test")
    public String customerDetails(){
        return "all details will come here ";
    }

    @PostMapping("/customer")
    public void addCustomer(@RequestBody Customer customer){
        customerRepo.save(customer);
    }
    @GetMapping("/customer/{id}")
    public Customer getCustomer(@PathVariable int id){
        return customerRepo.findById(id).get();
    }

    @PutMapping("/customer")
    public void updateCustomer(@RequestBody Customer customer){
        customerRepo.save(customer);
    }
}
